import Vuex from 'vuex'
import database from '~/plugins/saveData'

const createStore = () => {
  return new Vuex.Store({
    state: {
      config: {
        aseguradora: '',
        telefonoAS: '',
        grupoCallback: '',
        from: '',
        idPagina: 0,
        idCampana: 0,
        loading: false,
        urlNext:''
      },
      ejecutivo: {
        nombre: '',
        correo: '',
        id: 0
      },
      formData: {
        nombre: '',
        apellidoM: '',
        apellidoP: '',
        codigoPostal:'',
        telefono: '',
        correo: '',
        tipovivienda:'',
        tipopropietario:'',
        numeropisos:'',
        numerosotanos:'',
        aniosconstruccion:'',
        sumaasegurada:'',
      },
      solicitud: {},
      cotizacion: {},
      servicios: {
        //servicioDB: 'http://138.197.128.236:8081/ws-autos/servicios'
          servicioDB: 'http://192.168.10.65:8080/ws-rest/servicios'

      }
    },
    mutations: {
      saveData: function (state) {
        database.search(
          state.formData.apellidoM,
          state.formData.apellidoP,
          state.formData.codigoPostal,
          state.formData.correo,
          state.config.grupoCallback,
          state.config.idCampana,
          state.config.idPagina,
          state.formData.nombre,
          state.formData.telefono,
          state.formData.tipovivienda,
          state.formData.tipopropietario,
          state.formData.numeropisos,
          state.formData.numerosotanos,
          state.formData.aniosconstruccion,
          state.formData.sumaasegurada,
          state.config.telefonoAS
        )
          .then(resp => {
            state.solicitud = resp;
            state.ejecutivo.nombre = resp.nombreEjecutivo;
            state.ejecutivo.correo = resp.mailEjecutivo;
            state.ejecutivo.id = resp.idEjecutivo;
            $nuxt._router.push({path: state.config.urlNext})
          });
      },
    }
  })
}

export default createStore
