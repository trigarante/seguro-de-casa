import axios from 'axios'

const cotizacionService = {}

cotizacionService.search = function (apellidoM,
                                     apellidoP,
                                     codigoPostal,
                                     email,
                                     grupoCallback,
                                     idCampana,
                                     idPagina,
                                     nombre,
                                     telefono,
                                     tipovivienda,
                                     tipopropietario,
                                     numeropisos,
                                     numerosotanos,
                                     aniosconstruccion,
                                     sumaasegurada,
                                     telefonoAS) {

  return axios({
    method: "post",
    url: process.env.urlDB+  '/nuevaSolicitudHogar',
    data: {
      apellidoM,
      apellidoP,
      codigoPostal,
      email,
      grupoCallback,
      idCampana,
      idPagina,
      nombre,
      telefono,
      tipovivienda,
      tipopropietario,
      numeropisos,
      numerosotanos,
      aniosconstruccion,
      sumaasegurada,
      telefonoAS
    }
  })
    .then(res => res.data)
    .catch(err => console.error(err));
}
export default cotizacionService

